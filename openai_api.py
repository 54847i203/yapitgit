import sqlite3
import openai
import os
import time
from dotenv import load_dotenv
from db_connection import pg_history_from_db
load_dotenv()
openai_key = os.environ['OPENAI_KEY']




def _make_gptapi_history(system_str, name, x):
    print('********NAME', name)
    if not name.startswith('tsk-'):
        conversation_history = pg_history_from_db(name, x)
        prompts = []
        responses = []
        for conversation in conversation_history:
            prompts.append(conversation['prompt'])
            responses.append(conversation['best_choice_text'])
        messages = []
        if system_str != '':
            messages.append({"role": "system", "content": system_str})
        for i in range(len(prompts)):
            messages.append({"role": "user", "content": prompts[i]})
            messages.append({"role": "assistant", "content": responses[i]})
        return messages
    
    else:

        system_str = """you are a bot that just speaks Farsi and doesnt speak any other languages.
		you are a bot that makes a json data like below, from users conversation history which is needed for flights data retrival.
        every task you do is like this:
        you should asks user for mandatory data if not provided. 
        the json looks like this pattern from user's query.
"
		```
		data '{
		    "source": "THR",
		    "dest": "TBZ",
		    "sourceLabel": "تهران",
		    "destLabel": "تبریز",
		    "depart": "2023-04-26",
		    "adult": 1,
		    "child": 0,
		    "infant": 0,
		    "tripMode": 1,
		    "flightType": 2,
		    "errors": {
			"source": null,
			"dest": null,
			"depart": null,
			"return": null,
			"adult": null,
			"child": null,
			"infant": null
		    }
		```||FIN||"
		sourceLabel, destLabel, depart and number of people(adult, child and infant) are mandatory and should be asked from the user. 
        if sufficient mandatory values arent provided by user in the chat history for the last task, ask them until you gather the needed data.
        ask number of people(adult, child and infant) in just one question.
        if in any part of the process, user may ask you to cancel the reservation. in that case response with "||FIN||".
         once all the needed data is gathered,you ask the user to confirm the data. if its confirmed you return your response just as a valid json string to user without any explanation and add ||FIN|| tag to the end of your response. this indicates that this task is finished and you should gather data for another task. this response message shouldnt contain anything other than json string and tag.
		"""
        messages = [
            
                    {"role": "system", "content": system_str},
                    {"role": "user", "content": "سلام"},
                    {"role": "assistant", "content": "سلام! چطور می‌تونم به شما کمک کنم؟"},
                    {"role": "user", "content": "می خوام برم مسافرت"},
                    {"role": "assistant", "content": "عالیه! چه مقصدی رو در نظر دارید؟"},
                    {"role": "user", "content": "شیراز"},
                    {"role": "assistant", "content": "خوبه، کدام تاریخ می‌خواهید برای سفر به شیراز؟"},
                    {"role": "user", "content": "۲۲/۳/۲۰۲۳"},
                    {"role": "assistant", "content": "تعداد نفرات بزرگسال، کودک و نوزاد که همراهتان هستند را به من بگویید."},
                    {"role": "user", "content": "۳ بزرگسال هستیم با یک کودک ولی نوزاد همراهمون نیست"},
                    {"role": "assistant", "content": "خوبه، برای پرواز به شیراز واقع در ایران، مبدا خود را انتخاب کنید."},
                    {"role": "user", "content": "مبدا تهران است"},
                    {"role": "assistant", "content": """خوبه، اطلاعات شما به شرح زیر هستند:
                        - مبدا: تهران
                        - مقصد: شیراز
                        - تاریخ حرکت: ۲۲/۳/۲۰۲۳
                        - تعداد بزرگسال: ۳
                        - تعداد کودک: ۱
                        - تعداد نوزاد: ۰

                        آیا این اطلاعات درست هستند؟"""},
                    {"role": "user", "content": "بله صحیحه"},
                    {"role": "assistant", "content": """
                        ```
                        {
                            "source": "THR",
                            "dest": "SYZ",
                            "sourceLabel": "تهران",
                            "destLabel": "شیراز",
                            "depart": "2023-03-22",
                            "adult": 3,
                            "child": 1,
                            "infant": 0,
                            "tripMode": 1,
                            "flightType": 2,
                            "errors": {
                                "source": null,
                                "dest": null,
                                "depart": null,
                                "return": null,
                                "adult": null,
                                "child": null,
                                "infant": null
                            }
                        }
                        ```
                        ||FIN||"""}
                    ]
        conversation_history = pg_history_from_db(name, x)
        prompts = []
        responses = []
        for conversation in conversation_history:
            prompts.append(conversation['prompt'])
            responses.append(conversation['best_choice_text'])

        for i in range(len(prompts)):
            messages.append({"role": "user", "content": prompts[i]})
            messages.append({"role": "assistant", "content": responses[i]})
        print('*********messages', messages)
        return messages


def gpt_api(name, x, system_str , msg_content):


    history = _make_gptapi_history(system_str, name, x)
    current_message= [{"role": "user", "content": msg_content}]
    history.extend(current_message)
    print('x= ', x, '******MSG_CONTENT******', history)
    completion = openai.ChatCompletion.create(
        model='gpt-3.5-turbo',
        # messages=all_messages
        messages=history
        
    )
    print(completion)
    res = completion['choices'][0]['message']['content']
    print(res)
    return res



def playground(name, engine, prompt, max_tokens, n, stop, temperature,tick=False):
    print('name', name)
    print('prompt', prompt)
    print('engine', engine)
    openai.api_key = openai_key
    response = openai.Completion.create(
        engine=engine,
        prompt=prompt,
        
        max_tokens=int(max_tokens) if max_tokens is not None else 100,
        n=int(n) if n is not None else 1,
        stop=stop,
        temperature=temperature
    )
    def response_to_db(name, response, engine, prompt):
        conn = sqlite3.connect('database.db')
        c = conn.cursor()
        print(response)
        c.execute("CREATE TABLE IF NOT EXISTS playground(id TEXT, name TEXT, model TEXT, prompt TEXT, all_choices TEXT, best_choice_text TEXT, time INTEGER, tokens INTEGER, tick BOOL)")
        c.execute("INSERT INTO playground (id, name, model, prompt, all_choices, best_choice_text, time, tokens, tick) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", (response.id,name, engine, prompt, '', response.choices[0].text, int(response.created), int(response.usage.total_tokens), tick))
        conn.commit()
        conn.close()
    print(response.choices[0]['text'])


    def delete_db(name):
        try:
            conn = sqlite3.connect('database.db')
            cur = conn.cursor()
            sql = 'DROP TABLE IF EXISTS ' + name
            cur.execute(sql)
            conn.commit()
            conn.close()
        except:
            print('errror')
    
    # delete_db('playground')
    response_to_db(name, response, engine, prompt)

    return response.choices[0]['text']

def gpt_stream(name, msg_content, included_hist, system):
    full_msg = ''
    history = _make_gptapi_history(system, name, included_hist)
    current_message= [{"role": "user", "content": msg_content}]
    history.extend(current_message)
    print(f"history{history}")

    for completion in openai.ChatCompletion.create(
        model='gpt-3.5-turbo',
        messages=history,
        stream=True
    ):
        reason = completion['choices'][0]['finish_reason']
        if reason != None: 
            # WHEN FINISHED

            yield f"data: [DONE]\n\n"
            # DB COMMIT SHOULD BE CHANGED LATER

                

        if 'delta' in completion['choices'][0]:
            if 'content' in completion['choices'][0]['delta']:
                msg = completion['choices'][0]['delta']['content']
                full_msg = full_msg + msg

                msg = msg.replace('\n','~~~')
                yield f"data: {msg}\n\n"
    unix_time = int(time.time())
    print(unix_time)
    conn = sqlite3.connect('database.db')
    c = conn.cursor()
    print('tryin to save db', full_msg)
    c.execute("CREATE TABLE IF NOT EXISTS playground(id TEXT, name TEXT, model TEXT, prompt TEXT, all_choices TEXT, best_choice_text TEXT, time INTEGER, tokens INTEGER, tick BOOL)")
    c.execute("INSERT INTO playground (id, name, model, prompt, all_choices, best_choice_text, time, tokens, tick) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", ('',name, 'gpt-turbo', msg_content, '', full_msg, unix_time, 0, False))
    conn.commit()
    conn.close()

def gpt_nonstream(name, msg_content, included_hist, system):
    full_msg = ''
    history = _make_gptapi_history(system, name, included_hist)
    current_message= [{"role": "user", "content": msg_content}]
    history.extend(current_message)
    print(f"history{history}")

    completion = openai.ChatCompletion.create(
        model='gpt-3.5-turbo',
        messages=history,
        stream=False
    )
    res = completion['choices'][0]['message']['content']

    unix_time = int(time.time())
    print(unix_time)
    conn = sqlite3.connect('database.db')
    c = conn.cursor()
    print('tryin to save db', full_msg)
    c.execute("CREATE TABLE IF NOT EXISTS playground(id TEXT, name TEXT, model TEXT, prompt TEXT, all_choices TEXT, best_choice_text TEXT, time INTEGER, tokens INTEGER, tick BOOL)")
    c.execute("INSERT INTO playground (id, name, model, prompt, all_choices, best_choice_text, time, tokens, tick) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", ('',name, 'gpt-turbo', msg_content, '', full_msg, unix_time, 0, False))
    conn.commit()
    conn.close()
    return res

